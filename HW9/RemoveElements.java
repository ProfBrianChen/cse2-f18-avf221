//////////////////////////////////
///CSE 02 HW9 Remove ELements Anna Francisco
////////////////////////////////

import java.util.Scanner; //import classes
import java.util.Random;

public class RemoveElements{
  public static void main(String [] arg){ //main method from spec
	Scanner scan=new Scanner(System.in); 
int num[]=new int[10]; //new array
int newArray1[]; //array for after deleted method
int newArray2[]; //array for after removed method
int index,target; //integers as input for methods, input to delete, target number to remove from array
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]"); //creates an array of 10 random ints 0-9
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index "); //enter index to delete from array
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value "); //enter number to be deleted from array for every instance it occurs
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  
  public static int[] randomInput(){
    Random rand = new Random();
    int[] a = new int[10]; //assigning a[i] with a random integer 0-9 using a random object i created
    for (int i = 0; i<10; i++){
      int randInt = rand.nextInt(10);
      a[i] = randInt;
    }
    return a;
  }
    
    
  public static int[] delete(int[] a, int pos) {
   int[] deleted = new int[9];
   int j = 0;
   for (int i = 0; i<9; i++){
     if (i==pos){ //if index, shift forward one
       deleted[i]=a[i+1];
       j++; //counter to indicate the index is past the deleted one
     }
     if (j>0){ //after one is deleted, the rest will be shifted back one
       deleted[i]= a[i+1];
     }
     else{
       deleted[i] = a[i]; //before deleted index
     }
   }
    return deleted;
  }
  
  
    
    
  public static int[] remove(int[] a, int target){
    int targetFound = 0; //counter for later purposes in for loop
    int numTarget = 0; //count number of times target is found in array
    for (int i = 0; i<10; i++){
      if (a[i]==target){
        numTarget++;
      }
    }
    System.out.println("Number of " + target + "s found " + numTarget);
    
    
    int[] removed = new int[10-numTarget]; //create a new array the length of the original array minus how many targets we counted
    for (int i = 0; i<10; i++){
      //if(targetFound==numTarget){
      //  removed[i-targetFound]=a[i];
      //}

      if (a[i]==target) { //if we find the target , add one to counter, DONT ASSIGN ANYTHING, IGNORE
        targetFound++;
      }
      else if(a[i]!=target) { //if the current index is NOT the target, assign the index of removed at i minus target found to current index of a
       removed[i-targetFound]=a[i]; //
      }
      

      
      
      /*
      if (i>0&&a[i-1]==target&&a[i]!=target) {
        
      }
      if (a[i] != target) {
        removed[i]=a[i];
        
      }
      else if (a[i]==target){
        targetFound++;
        removed[i]=a[i];
        i++;
      }
      //if (targetFound>0) {
      //  i++;
      //}
      
*/
    }
    
    return removed;
  }
    
      
  
  
  
  
  
  
  
  
  
  
}
