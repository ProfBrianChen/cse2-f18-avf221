/////////////////////////////////////////
///CSE 02 HW9 CSE2Linear Anna Francisco
/////////////////////////////////////////
import java.util.Scanner; //import classes
import java.util.Arrays;
import java.util.Random;

public class CSE2Linear{
  public static void main(String args[]){
  Scanner scan = new Scanner(System.in); //Create scanner
  int[] a = new int[15]; //create new int array called a
  int b; //create grade integer
  int[] length = new int[1]; //create an array to help count iterations
  int[] length2 = new int[1]; //create an array to help count iterations in second 
    
  for (int i = 0; i<15; i++){
    System.out.println("Enter the next grade, ordered from least to greatest: "); //prompt
    
    
    
    while(!(scan.hasNextInt())){
      System.out.println("Error, not an integer. Please type an integer: "); //if not an int
      //b = scan.nextInt();
    }
    b = scan.nextInt();
    
    while(b<0||b>100){
      System.out.println("Error, not in range. Please enter an integer between 0 and 100 inclusive: "); //if not between 0 to 100
      b = scan.nextInt();
    }
    
    if (i>0) {
      while(b<=(a[i-1])) {
      System.out.println("Error, not bigger than previous. Please input an integer bigger than the last one inputted: "); //if not bigger than previous
      b = scan.nextInt();
      }
    }
    
    a[i]=b;
    
    

  }
    
    System.out.println(Arrays.toString(a)); //print array
    
    
    
    
    System.out.println("Enter a grade to be searched for: "); //prompt
    int key = scan.nextInt();
    int indKey = binarySearch2(a, key, length); //binary search with inputs
    if (indKey == -1){
      System.out.println("Grade not found."); 
    }
		else {
      System.out.println("Grade of " + key + " has been found. ");
    }
    System.out.println("Number of iterations: " + Arrays.toString(length)); //print number of iterations
    
    
    System.out.println();
    
    
    shuffle(a); //shuffle method
    int[] shuffled = new int[15]; //create new array for after shuffle method
    for (int j=0; j<15; j++) {
      shuffled[j] = a[j];
    }
    System.out.println("Shuffled array of Grades: ");
    System.out.println(Arrays.toString(shuffled)); //print shuffled array
    
    
    
    System.out.println("What grade would you like to search for?: "); //prompt for grade 
    int key2 = scan.nextInt(); 
    int indKey2 = linearSearch(shuffled, key2, length2); //invoke linear method
    if (indKey2 == -1) {
      System.out.println("Grade not found.");
    }
    else {
      System.out.println("Grade of " + key2 + " has been found. ");
    }
    System.out.println("Number of iterations: " + Arrays.toString(length2));
    
    

    
    
    
    
    
  }
  
  
  
  
  public static int binarySearch2(int[] list, int key, int[] length) { //took from class demo
		int low = 0;
		int high = list.length-1;
    length[0] = 1; //first iteration
		while(high >= low) {
			int mid = (low + high)/2; //middle element
			if (key < list[mid]) { //check if we have to search left half
				high = mid - 1;
			}
			else if (key == list[mid]) { //key has been found
				return mid;
			}
			else {
				low = mid + 1; //else is if we have to search right half
			}
      length[0] += 1; //means one more iteration since we went through without returning mid
		}
		return -1;
  
  }
  
    public static void printArray(String[] a){ //got from homework 8
    System.out.print(Arrays.toString(a)); //to string method to print a string array
    
  }
  
    public static int[] shuffle(int[] a){ //shuffle method from homework 8
    Random rand = new Random(); //random generator
      for (int j = 0; j<60; j++){
        int randInt = rand.nextInt(15); //pick random index 0-51
        int temp = a[0]; //set temp variable to hold first value
        a[0] = a[randInt]; //set first equal to random index value
        a[randInt] = temp; //set random index value to first value
      }
    return a;
  }
  
  public static int linearSearch(int[] list, int key, int[] length2) { 
    length2[0]=1; //first iteration
		for (int i = 0; i < list.length; i++) 
		{
			if (key == list[i]) { //key has been found
        return i;
      }
      length2[0] +=1; //to count iterations after first because key wasn't found
		}

	return -1;
	}
  
  
  
  
  
  
  
}