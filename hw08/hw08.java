/////////////////////////////////////////////
///CSE 002 HW 08 Card Shuffle Anna Francisco
/////////////////////////////////////////////

import java.util.Scanner; //import scanner and random classes
import java.util.Random;
import java.util.Arrays;

public class hw08 {
  

  public static void main(String[] args) { 
  Scanner scan = new Scanner(System.in); 
   //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"}; //array of suit names
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //array of card ranks
    
    
  String[] cards = new String[52]; //creating a String array with 52 spots allotted
  String[] hand = new String[5]; //creating a String array with 5 spots allotted
  String[] cards1 = new String[52]; //creating a String array with 52 spots allotted
  String[] hand2 = new String[5]; //creating a String array with 5 spots allotted
    
    
  System.out.println("Enter number of cards drawn in each hand: "); //prompt for number of cards drawn in each hand
    
  int numCards = scan.nextInt(); //num cards in a hand
  int again = 1; //setting a condition for while loop
  int index = 51; //last index of a deck of cards
    
  System.out.println("Cards in Order: ");  
  for (int i=0; i<52; i++){ //creating cards in order
    cards[i]=rankNames[i%13]+suitNames[i/13]; 
    System.out.print(cards[i]+" "); 
  } 
    
  System.out.println();
  System.out.println("Cards in Order: ");
  printArray(cards);
  System.out.println();
  
    
  String[] shuffled = shuffle(cards); //call shuffle method and set to string array
  System.out.println("Cards shuffled: ");
  System.out.println(Arrays.toString(shuffled)); //print shuffled
  System.out.println();
  again = 1;                
   while(again == 1){ 

       hand = getHand(cards,index,numCards); //hand method
       printArray(hand); //print hand
       System.out.println();
       index = index - numCards; //decrease index by numCards
       System.out.println("Enter a 1 if you want another hand drawn:");  //ask user if they want another hand
       again = scan.nextInt();  //condition
       if (index < numCards) { //once you run out of enough cards to print the number of cards in a hand
         //while (again ==1) {
         //System.out.println("New deck.");  
         index = 51; //reset index to 51
         cards = makeDeck(); //create new deck
         printArray(cards); //print new deck 
         System.out.println(); //print spaces for clarity and readability
         System.out.println();
         //hand = getHand(cards1,index,numCards); 
         //printArray(hand2);
         //System.out.println();
         //index = index - numCards;
         //System.out.println("Enter a 1 if you want another hand drawn:"); 
         //again = scan.nextInt();
       //}
       }
    }  
  }
  
                     
                     
                     
  public static void printArray(String[] a){
    /*for (int i=0; i< 52; i++){
      System.out.print(a[i]);
      System.out.print(" ");
    }
    System.out.println(); */
    System.out.print(Arrays.toString(a)); //to string method to print a string array
    
  }
  
                     
                     
                     
  public static String[] shuffle(String[] a){ //shuffle method
    Random rand = new Random(); //random generator
      for (int j = 0; j<60; j++){
        int b = rand.nextInt(52); //pick random index 0-51
        String temp = a[0]; //set temp variable to hold first value
        a[0] = a[b]; //set first equal to random index value
        a[b] = temp; //set random index value to first value
      }
    return a;
  }
  
  
                     
                     
                     
  public static String[] getHand(String[] a, int b, int c){ //b is index , c is numCards
    int startingIndex = b;
    String[] temp = new String[c];  //return string
    int j = 0;
    //System.out.println("In get hand method");
    for (int i = startingIndex; j<c ;i--) {      
      //if (j<5){
      temp[j] = a[i]; //set each temp array index to the last (numCards) cards in array
      startingIndex -= c; //decrease index by numCards
      j++;
      }
    //}
    return temp; 
  }
        
  
  
  
  
  public static String[] makeDeck(){ //making a new deck
    String[] suitNames={"C","H","S","D"}; //array of suit names
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //array of card ranks
    
    
    String[] cards2 = new String[52]; //creating a String array with 52 spots allotted
    String[] hand2 = new String[5]; //creating a String array with 5 spots allotted
    System.out.print("New deck: ");
    for (int p=0; p<52; p++){ //creating cards in order
      cards2[p]=rankNames[p%13]+suitNames[p/13]; 
      System.out.print(cards2[p]+" "); 
      } 
    System.out.println();
    return cards2; //return new deck
  }                   
                     
                     
                     
                     
}


