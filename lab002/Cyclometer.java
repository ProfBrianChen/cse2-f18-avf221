////////////////////////
/// CSE Lab 2 Cyclometer
////////////////////////

public class Cyclometer{
  
  public static void main(String args[]){
    //enter inputs for time elapsed in seconds for both trips
	  int secsTrip1=480;  //seconds in trip 1
    int secsTrip2=3220;  //seconds in trip 2
    //enter inputs for distances 
		int countsTrip1=1561;  //distance trip 1
		int countsTrip2=9037; //distance trip 2
    
    double wheelDiameter=27.0,  //diameter of wheel in inches
  	PI=3.14159, //represents pi with a few decimals
  	feetPerMile=5280,  //number of feet in a mile
  	inchesPerFoot=12,   //conversion factor of inches in a foot
  	secondsPerMinute=60;  //conversion factor of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //initializing distances for each trip and total distance
    
    
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute) +" minutes and had "+ countsTrip1+" counts."); //print time in minutes and counts for trip 1
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");//print time in minutes and counts for trip 2 
    
    //calculutating the distance traveled using the number of rotations, the diameter, and pi
		//
		//
		//
	  distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
    
    	//Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");

  }
  
  
  
}