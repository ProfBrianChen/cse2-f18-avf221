/////////////////////////////
///// CSE 02 Homework 4 Craps Using If Statements
/////////////////////////////

import java.util.Scanner; //import scanner class
import java.util.Random; //import random class

public class CrapsIf{ //analyzing pairs of dice using if and else if statments and printing the name of them in the game of craps
  
  public static void main(String args[]){
    Scanner askUser = new Scanner (System.in); //create scanner called askUser
    Random dice = new Random(); //create random generator called dice
    System.out.print("Would you like to get two random numbers for the die roll (type in 1), or input your two numbers (type in 2) ?"); //ask user which method they want to input two die numbers
    int type = askUser.nextInt(); //scan in the input from user
    int die1, die2; //initialize both integers for die numbers
    
    
    
    if (type==1){ //if the user says they want to generate the numbers randomly
      die1 = dice.nextInt(6)+1; //die one, generating an integer one to six
      die2 = dice.nextInt(6)+1; //die two, generating an integer one to six
      System.out.println("Die 1: " + die1); //print die numbers
      System.out.println("Die 2: " + die2);
      if (die1==1){ //in the cases that one die is a one
        if(die2==1){
          System.out.println("Snake eyes");
        }
        else if (die2==2){
          System.out.println("Ace Deuce");
        }
        else if (die2==3){
          System.out.println("Easy Four");
        }
        else if (die2==4){
          System.out.println("Fever Five");
        }
        else if (die2==5){
          System.out.println("Easy Six");
        }
        else if (die2==6){
          System.out.println("Seven Out");
        }
      }
      if (die1==2){ //cases one die is a 2
        if (die2==1){
          System.out.println("Ace Deuce");
        }
        else if (die2==2){
          System.out.println("Hard Four");
        }
        else if (die2==3){
          System.out.println("Fever Five");
        }
        else if (die2==4){
          System.out.println("Easy Six");
        }
        else if (die2==5){
          System.out.println("Seven Out");
        }
        else if (die2==6){
          System.out.println("Easy Eight");
        }
      }
      if (die1==3){ //cases one die is a 3
        if (die2==1){
          System.out.println("Easy Four");
        }
        else if (die2==2){
          System.out.println("Fever Five");
        }
        else if (die2==3){
          System.out.println("Hard Six");
        }
        else if (die2==4){
          System.out.println("Seven Out");
        }
        else if (die2==5){
          System.out.println("Easy Eight");
        }
        else if (die2==6){
          System.out.println("Nine");
        }
      }
      if (die1==4){ //cases one die is a 4
        if (die2==1){
          System.out.println("Fever Five");
        }
        else if (die2==2){
          System.out.println("Easy Six");
        }
        else if (die2==3){
          System.out.println("Seven Out");
        }
        else if (die2==4){
          System.out.println("Hard Eight");
        }
        else if (die2==5){
          System.out.println("Nine");
        }
        else if (die2==6){
          System.out.println("Easy Ten");
        }
      }
      if (die1==5){ //cases one die is a 5
        if (die2==1){
          System.out.println("Easy Six");
        }
        else if (die2==2){
          System.out.println("Seven Out");
        }
        else if (die2==3){
          System.out.println("Easy Eight");
        }
        else if (die2==4){
          System.out.println("Nine");
        }
        else if (die2==5){
          System.out.println("Hard Ten");
        }
        else if (die2==6){
          System.out.println("Yo-leven");
        }
      }
      if (die1==6){ //cases one die is a 6
        if (die2==1){
          System.out.println("Seven Out");
        }
        else if (die2==2){
          System.out.println("Easy Eight");
        }
        else if (die2==3){
          System.out.println("Seven Out");
        }
        else if (die2==4){
          System.out.println("Easy Ten");
        }
        else if (die2==5){
          System.out.println("Yo-leven");
        }
        else if (die2==6){
          System.out.println("Boxcars");
        }
      }
    }
    
    
    
    
    
    
    
    else if (type==2){
      System.out.print("What is the first die number?: "); //ask for first die number
      die1 = askUser.nextInt(); //scan in die number 1
      System.out.print("What is the second die number?: "); //ask for second die numbers
      die2 = askUser.nextInt(); //scan in die number 2
      System.out.println("Die 1: " + die1);
      System.out.println("Die 2: " + die2);
      if (die1==1){ //same cases repeated, except the method was by input
        if(die2==1){
          System.out.println("Snake eyes");
        }
        else if (die2==2){
          System.out.println("Ace Deuce");
        }
        else if (die2==3){
          System.out.println("Easy Four");
        }
        else if (die2==4){
          System.out.println("Fever Five");
        }
        else if (die2==5){
          System.out.println("Easy Six");
        }
        else if (die2==6){
          System.out.println("Seven Out");
        }
      }
      if (die1==2){
        if (die2==1){
          System.out.println("Ace Deuce");
        }
        else if (die2==2){
          System.out.println("Hard Four");
        }
        else if (die2==3){
          System.out.println("Fever Five");
        }
        else if (die2==4){
          System.out.println("Easy Six");
        }
        else if (die2==5){
          System.out.println("Seven Out");
        }
        else if (die2==6){
          System.out.println("Easy Eight");
        }
      }
      if (die1==3){
        if (die2==1){
          System.out.println("Easy Four");
        }
        else if (die2==2){
          System.out.println("Fever Five");
        }
        else if (die2==3){
          System.out.println("Hard Six");
        }
        else if (die2==4){
          System.out.println("Seven Out");
        }
        else if (die2==5){
          System.out.println("Easy Eight");
        }
        else if (die2==6){
          System.out.println("Nine");
        }
      }
      if (die1==4){
        if (die2==1){
          System.out.println("Fever Five");
        }
        else if (die2==2){
          System.out.println("Easy Six");
        }
        else if (die2==3){
          System.out.println("Seven Out");
        }
        else if (die2==4){
          System.out.println("Hard Eight");
        }
        else if (die2==5){
          System.out.println("Nine");
        }
        else if (die2==6){
          System.out.println("Easy Ten");
        }
      }
      if (die1==5){
        if (die2==1){
          System.out.println("Easy Six");
        }
        else if (die2==2){
          System.out.println("Seven Out");
        }
        else if (die2==3){
          System.out.println("Easy Eight");
        }
        else if (die2==4){
          System.out.println("Nine");
        }
        else if (die2==5){
          System.out.println("Hard Ten");
        }
        else if (die2==6){
          System.out.println("Yo-leven");
        }
      }
      if (die1==6){
        if (die2==1){
          System.out.println("Seven Out");
        }
        else if (die2==2){
          System.out.println("Easy Eight");
        }
        else if (die2==3){
          System.out.println("Seven Out");
        }
        else if (die2==4){
          System.out.println("Easy Ten");
        }
        else if (die2==5){
          System.out.println("Yo-leven");
        }
        else if (die2==6){
          System.out.println("Boxcars");
        }
      }   
    }
    
    
    
    
    
    
    
    
    
    
    else { //if the user picked a number that was not specified
      System.out.println("That was not a valid option");
    }
    
    
    
  }
}