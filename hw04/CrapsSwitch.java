/////////////////////////////
///// CSE 02 Homework 4 Craps Using Switch Statements
/////////////////////////////


import java.util.Scanner; //import scanner class
import java.util.Random; //import random class

public class CrapsSwitch { //analyzing pairs of dice using switch statments and printing the name of them in the game of craps
  
  public static void main(String args[]){
    Scanner askUser = new Scanner (System.in); //create scanner called askUser
    Random dice = new Random(); //create random generator called dice
    System.out.print("Would you like to get two random numbers for the die roll (type in 1), or input your two numbers (type in 2) ?"); //ask user which method they want to input two die numbers
    int type = askUser.nextInt(); //scan in the input from user
    int die1, die2; //initialize two ints for the die numbers
    
    if (type==1){ //if the user says they want to generate the numbers randomly
      die1 = dice.nextInt(6)+1; //die one, generating an integer one to six
      die2 = dice.nextInt(6)+1; //die two, generating an integer one to six
      System.out.println("Die 1: " + die1); //print both die numbers to screen
      System.out.println("Die 2: " + die2);
      if (die1==1){ //using switch statements to analyze the pair of die and printing the name to the screen
        switch (die2) {
          case 1 :
          System.out.println("Snake eyes");
          break;
          case 2: 
          System.out.println("Ace Deuce");
          break;
          case 3: 
          System.out.println("Easy Four");
          break;
          case 4: 
          System.out.println("Fever Five");
          break;
          case 5: 
          System.out.println("Easy Six");
          break;
          case 6: 
          System.out.println("Seven Out");
          break;
        }
      }
      if (die1==2){ //the case that one die is a 2
        switch (die2){
          case 1:
          System.out.println("Ace Deuce");
          break;
          case 2:
          System.out.println("Hard Four");
          break;
          case 3: 
          System.out.println("Fever Five");
          break;
          case 4:
          System.out.println("Easy Six");
          break;
          case 5:
          System.out.println("Seven Out");
          break;
          case 6: 
          System.out.println("Easy Eight");
          break;
      }
    }
      if (die1==3){ //the case that one of the die is a 3
        switch (die2){
          case 1:
          System.out.println("Easy Four");
          break;
          case 2:
          System.out.println("Fever Five");
          break;
          case 3:
          System.out.println("Hard Six");
          break;
          case 4: 
          System.out.println("Seven Out");
          break;
          case 5:
          System.out.println("Easy Eight");
          break;
          case 6:
          System.out.println("Nine");
          break;
      }
    }
      if (die1==4){ //the case that one of the die is a 4
        switch(die2){
          case 1:
          System.out.println("Fever Five");
          break;
          case 2:
          System.out.println("Easy Six");
          break;
          case 3:
          System.out.println("Seven Out");
          break;
          case 4:
          System.out.println("Hard Eight");
          break;
          case 5:
          System.out.println("Nine");
          break;
          case 6:
          System.out.println("Easy Ten");
          break;
        }
      }
      if (die1==5){ //the case that one of the die is a 5
        switch (die2){
          case 1:
          System.out.println("Easy Six");
          break;
          case 2:
          System.out.println("Seven Out");
          break;
          case 3:
          System.out.println("Easy Eight");
          break;
          case 4:
          System.out.println("Nine");
          break;
          case 5:
          System.out.println("Hard Ten");
          break;
          case 6:
          System.out.println("Yo-leven");
          break;
        }
      }
      if (die1==6){ //the case that one of the die is a 6
        switch(die2){
          case 1:
          System.out.println("Seven Out");
          break;
          case 2:
          System.out.println("Easy Eight");
          break;
          case 3:
          System.out.println("Seven Out");
          break;
          case 4:
          System.out.println("Easy Ten");
          break;
          case 5:
          System.out.println("Yo-leven");
          break;
          case 6:
          System.out.println("Boxcars");
          break;
      }
    }
    }
    
    
    
    
    
    
    else if (type==2){ //repeat all the cases but for the input method
      System.out.print("What is the first die number?: "); //ask for first die number
      die1 = askUser.nextInt(); //scan in die number 1
      System.out.print("What is the second die number?: "); //ask for second die numbers
      die2 = askUser.nextInt(); //scan in die number 2
      System.out.println("Die 1: " + die1);
      System.out.println("Die 2: " + die2);
       if (die1==1){
        switch (die2) {
          case 1 :
          System.out.println("Snake eyes");
          break;
          case 2: 
          System.out.println("Ace Deuce");
          break;
          case 3: 
          System.out.println("Easy Four");
          break;
          case 4: 
          System.out.println("Fever Five");
          break;
          case 5: 
          System.out.println("Easy Six");
          break;
          case 6: 
          System.out.println("Seven Out");
          break;
        }
      }
      if (die1==2){
        switch (die2){
          case 1:
          System.out.println("Ace Deuce");
          break;
          case 2:
          System.out.println("Hard Four");
          break;
          case 3: 
          System.out.println("Fever Five");
          break;
          case 4:
          System.out.println("Easy Six");
          break;
          case 5:
          System.out.println("Seven Out");
          break;
          case 6: 
          System.out.println("Easy Eight");
          break;
      }
    }
      if (die1==3){
        switch (die2){
          case 1:
          System.out.println("Easy Four");
          break;
          case 2:
          System.out.println("Fever Five");
          break;
          case 3:
          System.out.println("Hard Six");
          break;
          case 4: 
          System.out.println("Seven Out");
          break;
          case 5:
          System.out.println("Easy Eight");
          break;
          case 6:
          System.out.println("Nine");
          break;
      }
    }
      if (die1==4){
        switch(die2){
          case 1:
          System.out.println("Fever Five");
          break;
          case 2:
          System.out.println("Easy Six");
          break;
          case 3:
          System.out.println("Seven Out");
          break;
          case 4:
          System.out.println("Hard Eight");
          break;
          case 5:
          System.out.println("Nine");
          break;
          case 6:
          System.out.println("Easy Ten");
          break;
        }
      }
      if (die1==5){
        switch (die2){
          case 1:
          System.out.println("Easy Six");
          break;
          case 2:
          System.out.println("Seven Out");
          break;
          case 3:
          System.out.println("Easy Eight");
          break;
          case 4:
          System.out.println("Nine");
          break;
          case 5:
          System.out.println("Hard Ten");
          break;
          case 6:
          System.out.println("Yo-leven");
          break;
        }
      }
      if (die1==6){
        switch(die2){
          case 1:
          System.out.println("Seven Out");
          break;
          case 2:
          System.out.println("Easy Eight");
          break;
          case 3:
          System.out.println("Seven Out");
          break;
          case 4:
          System.out.println("Easy Ten");
          break;
          case 5:
          System.out.println("Yo-leven");
          break;
          case 6:
          System.out.println("Boxcars");
          break;
      }
    }
    }
      
    
    
    
    
    
    
    
    
    
    else { //the case that the user inputs a number that wasnt an option
      System.out.println("That was not a valid option");
    }
    
    
    
 
  }
}