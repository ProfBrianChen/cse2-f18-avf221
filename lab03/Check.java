////////////////////
//// CSE 02 Lab 3 Splitting Check at Dinner
/// 
import java.util.Scanner;
public class Check{
  
  
    
  public static void main(String args[]){

    
  Scanner myScanner = new Scanner( System.in ); //create scanner
  System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt user for total cost of check
  double checkCost = myScanner.nextDouble();//scan in cost
  
  System.out.print("Enter the percentage tip that you wish to pay as a whole number ,in the form xx: ");  //prompt for tip percentage you wish to pay
  double tipPercent = myScanner.nextDouble(); //scan in percentage
  tipPercent /= 100; //We want to convert the percentage into a decimal value
    
  System.out.print("Enter the number of people who went out to dinner: "); //prompt for number of people
  int numPeople = myScanner.nextInt(); //scan in number
  
  
  double totalCost;
  double costPerPerson;
  int dollars;   //whole dollar amount of cost 
  int dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
  totalCost = checkCost * (1 + tipPercent);
  costPerPerson = totalCost / numPeople;
  //get the whole amount, dropping decimal fraction
  dollars=(int)costPerPerson;
  //get dimes amount, e.g., 
  // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
  //  where the % (mod) operator returns the remainder
  //  after the division:   583%100 -> 83, 27%5 -> 2 
  dimes=(int)(costPerPerson * 10) % 10;
  pennies=(int)(costPerPerson * 100) % 10;
  System.out.println("Each person in the group owes $$" + dollars + "." + dimes + pennies);

  



  
  
  
  

    
  }
}
