////////////////////////////
///CSE 02 HW 5 Poker Hands
////////////////////////////
import java.util.Scanner; //import Scanner and random class
import java.util.Random;

public class hw05{
  public static void main(String args[]){
    
    int numHands; //initialize number of hands and final probability variables
    double probFour;
    double probThree;
    double probTwoPair;
    double probOnePair;
    
    int thisIsFour = 0; //initialize temporary identifiers that will count what each hand is
    int thisIsThree = 0; //will set to 0 at the beginning of every loop
    int thisIsTwo = 0;
    int thisIsOne = 0;
    
    int countFour = 0; //initialize counters for each hand type
    int countThree = 0;
    int countTwo = 0;
    int countOne = 0;
    
    Random rand = new Random(); //create random generator and scanner
    Scanner scan = new Scanner(System.in);
    
    int rand1; //initialize 5 random card numbers generated
    int rand2;
    int rand3;
    int rand4;
    int rand5;
    
    
    System.out.print("Enter number of hands drawn: "); //prompt for hands drawn
    numHands = scan.nextInt(); //scan in how many hands will be drawn
    System.out.println("");
    
   
    for (int i=0; i < numHands; ++i) { //create for loop to iterate the amount of times that the user inputted
      rand1 = rand.nextInt(52)+1; //create 5 random integers between 1 to 52 inclusive
      rand2 = rand.nextInt(52)+1;
      rand3 = rand.nextInt(52)+1;
      rand4 = rand.nextInt(52)+1;
      rand5 = rand.nextInt(52)+1;
      //System.out.print(rand1 + " " + rand2 + " " + rand3 + " " + rand4 + " " +rand5 + " : ");
      
      thisIsOne = 0; //make all counters 0 for the start of counting each new hand
      thisIsTwo = 0;
      thisIsThree = 0;
      thisIsFour = 0;
      
      
      ////////////
      //CASES WHERE FIRST TWO MATCH
        if (rand1 % 13 == rand2 % 13) { //this is if the first two match
          
                 //cases where first three match
                if (rand2 % 13 == rand3 % 13) { //then considering if it's three of a kind
                     if (rand3 % 13 == rand4 % 13) { //then considering if it's four of a kind
                      thisIsFour += 1; //add one to temporary counter for four of a kind
                     }
                     else if (rand3 % 13 == rand5 % 13) { //if all match but number 4
                      thisIsFour +=1;
                     }
                     else if (rand3 %13 != rand4 %13 && rand3 %13 != rand5 %13 ) { //doesn't count full houses
                      thisIsThree += 1;
                     }
                     else if ((rand3 %13 != rand4 %13 && rand3 %13 != rand5 %13)  && rand4 %13 == rand5 %13) { //full house, counts a pair, counts three of a kind
                       thisIsOne +=1;
                       thisIsThree +=1;
                     }
                 }
          
                //cases where first two match, but don't match third one
               else if (rand3 % 13 == rand4 % 13) { //if third does not match, but 3rd and 4th card match
                  if (rand1 % 13 != rand5% 13 && rand4 %13 != rand5 %13) { //preventing counting a full house
                    thisIsTwo += 1;  //two pair
                  }
                  else if (rand1 % 13 == rand5% 13 && rand4 %13 == rand5 %13) { //full house
                    thisIsOne +=1;
                    thisIsThree +=1;
                  }
               }  
               else if (rand4%13 == rand5%13) {
                thisIsTwo +=1;
              }
        
        }
           
          
      
      
      
      
      
           
      
           /////////
     ////CASES WHERE FIRST TWO DON'T MATCH, SECOND AND THIRD DO
     else if (rand2 % 13 == rand3 % 13) { //if first two don't match, but 2nd and 3rd do
          
                //2nd 3rd 4th match
                if (rand3 % 13 == rand4 % 13) { //2nd 3rd and 4th match
                    if (rand4 % 13 == rand5 % 13) { //if all the last 4 cards do
                      thisIsFour += 1;
                    }
                    else if (rand1 % 13 != rand5 %13){ //not counting a full house
                      thisIsThree += 1;
                    }
                    else if (rand1 % 13 == rand5 %13) { //full house, counts a pair and a three of a kind
                      thisIsThree +=1;
                      thisIsOne +=1;
                    }
                }
          
                //if 2nd and 3rd match, but 3rd and 4th dont match
                else if (rand4 % 13 == rand5 % 13 && rand1 %13 != rand4 %13) { //not a full house
                  thisIsTwo += 1;
                }
                else if (rand1 % 13 == rand4 % 13 && rand1 %13 != rand5 %13) {  //not a full house
                  thisIsTwo += 1;
                }
                else if (rand1 % 13 == rand4 % 13 && rand1 %13 == rand5 %13) { //full hosue
                  thisIsThree +=1;
                  thisIsOne +=1;
                }
                else {
                  thisIsOne += 1;
                }
        }
      
      
      
      
      
      
      ///////////
      ////CASES WHERE FIRST THREE DONT MATCH , 3rd and 4th match
        else if (rand3 % 13 == rand4 % 13) {
               if (rand4 % 13 == rand5 % 13) {
                  thisIsThree += 1;
                }
               else if (rand1 % 13 == rand5 % 13) { //checking if there is another pair
                  thisIsTwo +=1;
                }
               else if (rand2 % 13 == rand5 % 13) { //checking if there is another pair since one and two don't match
                  thisIsTwo +=1;
                }
                else {
                  thisIsOne += 1;
                }     
        }

      
      
            ////////////
      ///CASES WHERE FIRST FOUR DONT MATCH, 4TH AND FIFTH MATCH
      else if (rand4%13 ==rand5%13) {
        thisIsOne +=1;
      }
      
      
      
          
        //////////
        ///CASES WHERE FIRST TWO DONT MATCH, BUT 1ST AND 3RD MATCH
        else if (rand1 % 13 == rand3 % 13) {
          if (rand3 %13 ==rand4 %13 && rand3 %13 ==rand5 %13) { //first third fourth and fifth match
            thisIsFour +=1;
          }
          else if (rand3 %13 ==rand4 %13 && rand3 %13 !=rand5 %13) { //first third and fourth match
            thisIsThree +=1;
          }
          else if (rand3 %13 ==rand4 %13 && rand3 %13 !=rand5 %13 && rand2 %13 == rand5%13) {//full house
            thisIsThree +=1;
            thisIsOne +=1;
          } 
          else if (rand1%13 != rand4%13 && rand2%13 != rand4%13 && rand4%13 == rand5%13) {//first and third match, fourth and fifth match
            thisIsTwo +=1;
          }
          else if (rand2%13 ==rand4%13 && rand2%13 != rand5%13) { //first and third match, second and fourth match, fifth side card
            thisIsTwo +=1;
          }
          else if (rand2%13 ==rand4%13 && rand2%13 == rand5%13) { //first and third match, second fourth and fifth match
            thisIsThree +=1;
            thisIsOne +=1;
          }
          
        }
     
          
      
    
      /////////
      ////CASES WHERE 2ND AND 4TH MATCH
      else if (rand2%13 == rand4%13) {
        if (rand2 %13 ==rand5%13) {
          thisIsThree +=1;
        }
        else if (rand1 % 13 != rand5%13 && rand3%13 != rand5%13) {
          thisIsOne +=1;
        }
        else if (rand1%13 ==rand5%13) {
          thisIsTwo +=1;
        }
        else if (rand3%13==rand5%13) {
          thisIsTwo +=1;
        }
      }
      
    
      
      
      ////////////////
      //// CASES WHERE 3RD AND 5TH MATCH
      
      else if (rand3 % 13 == rand5 %13) {
        thisIsOne +=1;
      }
      
      
      
  
        //////////
        //CASES WHERE FIRST THREE DONT MATCH BUT 1ST AND FOURTH DO
        else if (rand1 % 13 == rand4%13) {
          if (rand4%13 != rand5 % 13) {
            thisIsOne +=1;
          }
          else if (rand4%13 != rand5 % 13) {
            thisIsThree +=1;
          }
          else if (rand2 %13 ==rand5%13) {
            thisIsTwo +=1;
          }
          else if (rand3%13 ==rand5%13) {
            thisIsTwo+=1;
          }
        }
          
       
   
      
      ////////
      ////CASES WHERE 2ND AND FIFTH MATCH
      else if (rand2%13 == rand5%13) {
        thisIsOne +=1;
      }
      
      
      
      
      
      /////////
      ///CASES WHERE ALL DONT MATCH EXCEPT FIRST AND FIFTH
      //////////
      else if (rand1 %13 == rand5%13) {
        thisIsOne +=1;
      }
      
      
      

      
      
     if (thisIsOne > 0) {
        //System.out.println("This is a one pair.");
        countOne +=1;
      }
     if (thisIsTwo > 0) {
        //System.out.println("This is a two pair.");
        countTwo +=1;
      }
     if (thisIsThree > 0){
        //System.out.println("This is a three of a kind.");
        countThree +=1;
      }
      if (thisIsFour > 0){
        //System.out.println("This is a four of a kind.");
        countFour +=1;
      }
      
      //System.out.println();
      
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Number of four of a kinds: " + countFour); //print out how many of each kind
    System.out.println("Number of three of a kinds: " +countThree);
    System.out.println("Number of two pairs: " + countTwo);
    System.out.println("Number of one pairs: " + countOne);
    
    probFour = (double) countFour/ (double) numHands; //calculate probabilities by explicitly casting
    probThree = (double)countThree/ (double)numHands; 
    probTwoPair = (double) countTwo/ (double) numHands;
    probOnePair = (double) countOne/ (double) numHands;
  
    System.out.println("The number of hands: " + numHands); //print number of hands
    System.out.println("Probability of four-of-a-kind: " + probFour); //print out the probabilities of each kind
    System.out.println("Probability of three-of-a-kind: " + probThree); 
    System.out.println("Probability of two-pair: " + probTwoPair);
    System.out.println("Probability of one-pair: " + probOnePair);
    System.out.println();
    
    
    
  
  }
}