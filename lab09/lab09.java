/////////////////////////////////////////
//CSE 02 Lab 09 Arrays Anna Francisco///
/////////////////////////////////////////
//import java.util.Scanner;
import java.util.Arrays;
//import java.util.Random;

public class lab09 {
  
  
  public static void main(String args[]){
  int[] array0 = {0,9,7,7,15,90,32,45}; //declare original array
  System.out.print("Array 0: ");
  System.out.println(Arrays.toString(array0)); //print it for checking purposes
    
    
  int[] array1 = copy(array0); //assign a copy of original to array1
  System.out.print("Copy-- Array 1: ");
  System.out.println(Arrays.toString(array1)); //print for checking purposes
    
    
  int[] array2 = copy(array0); //assign a copy of original to array2
  System.out.print("Copy-- Array 2: ");
  System.out.println(Arrays.toString(array2));  //print for checking purposes
    
  inverter(array0); //invert array 0
  System.out.print("Array 0 inverted: ");
  print(array0); //use print method
    
  inverter2(array1); //invert array1 with inverter 2 but since we assigned a copy to new memory, it does not invert when printed
  System.out.print("Array 1 inverted: ");
  print(array1);  //use print method for array1
    
  int[] array3 = inverter2(array2); //pass array2 to inverter2 method and assign to array3, since we're assigning it, printing array3 will be inverted
  System.out.print("Array 3: "); 
  print(array3); //print method for array 3
    
  /*int[] array3 = new int[array2.length];
  for (int i=0; i<array2.length; i++){
    array3[i] = array2[i];
  }
  System.out.print("Array 3: ");
  print(array3);*/
  }
  
  
  /*Create a method called copy(), which accepts an integer array as input, 
  and returns an integer array as output.  It should declare and allocate 
  a new integer array that is the same length as the input, use a for-loop 
  to copy each member of the input to the same member in the new array.  
  Finally, it should return the new array as output. */
  public static int[] copy(int[] a){
    int[] copied = new int[a.length];
    for (int i=0; i<a.length; i++){
      copied[i] = a[i];
    }
    return copied;
  }
  
  
  
  
  /*Create a method, inverter(), which reverses the order of an array.  
  Inverter should work by accepting an array of integers as input.  Then
  it should systematically swap the first member of the input array with
  the last member, the second member of the input array with the 
  second-to-last member, and so on.  Therefore it modifies the memory 
  pointed to by the array input.  It returns void.*/
  public static void inverter(int[] a) {
    for (int i=0; i<a.length-1; i++) {
    int temp = 0;
    temp= a[i];
    a[i] = a[a.length-i-1];
    a[a.length-i-1] = temp;
    }
    //System.out.println("Went through inverter");
  }
  
  
  /*Create a second method, inverter2(), which first uses copy() to make
  a copy of the input array.  Then it uses a copy of the code from
  inverter() to invert the members of the copy.  Finally, it returns 
  the copy as output.*/
  public static int[] inverter2(int[] a){
    //System.out.println(Arrays.toString(copy(a)));
    int[] b  = copy(a);
    inverter(b);
    return b;
  }
  
  
  /*Create a method called print() which accepts any integer array as 
  input, and returns nothing.  Use a for-loop to print all members of 
  the array.*/
  public static void print(int[] a){
    //System.out.print("\[");
    for (int i=0; i<a.length; i++){
      System.out.print(a[i]);
      if (i<a.length-1){
      System.out.print(", ");
      }
    }
    //System.out.print("\]");
    System.out.println();
  }
  
  
  
  
  
  
  
  
}










