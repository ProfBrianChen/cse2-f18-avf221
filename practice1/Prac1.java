


import java.util.Scanner;

public class Prac1 {
  public static void main (String args[]){
    Scanner scan = new Scanner(System.in);
    System.out.print("Input a one, two, or three bit binary number: ");
    int a = scan.nextInt();
    int x, y, z;
    x = y = z = 0;
    if (a==0 || a==1 || a==10 || a==11 || a==100 || a==101 || a==110 || a==111) {
      x = a/100;
      y = a% 100 / 10;
      z = a % 10;
    }
    else {
      System.out.println("You entered an invalid number.");
    }
    
    int result = (x*4) + (y*2) + (z*1);
    System.out.println(result);
  }
}