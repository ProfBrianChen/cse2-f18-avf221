/////////////////////////
//////CSE HW 06
/////////////////////////


import java.util.Scanner; //import scanner class
 
public class EncryptedX{
  public static void main(String args[]) {
   
    Scanner scan = new Scanner(System.in); //create scanner
    //System.out.print("Enter ASCII Letter: "); //prompt for letter
    //String temp = scan.next(); //create a temporary string for the character
		//char result = temp.charAt(0); //get the character from string
    //System.out.println("ASCII character entered: " + result); //print for checking purposes
   
    String firstBlank = new String(" ");
    String lastBlank = new String(" ");
    String midBlank = new String(" ");
    
    String begStars = new String("");
    String midStars = new String("");
    String endStars = new String("");
    
    
    System.out.print("Enter a number 0 through 100 inclusive: "); //prompt for number of generations
    int gen = scan.nextInt(); //scan
    while (gen>100 || gen<0) { //check number of generations is between 1 to 5
      System.out.println("Not a valid number."); 
      System.out.print("Enter number 0 through 100: "); //reprompt
      gen = scan.nextInt(); //rescan
    }  
    System.out.println("Number entered: " + gen); //print for checking purposes
    

    
    //FIRST ATTEMPT
    /*for (int i=0; i<(gen/2.0); i++){ //creating int for beginning stars
      if (i>0){
        begStars += "*";
      }
      System.out.println(begStars + firstBlank + midStars + secondBlank + endStars);
    }
    for (int k=gen-1; k>0; k -=2 ) {
      midStars += "*";
      System.out.println(begStars + firstBlank + midStars + secondBlank + endStars); 
    }
      for (int m=0; m<(gen/2.0); m++) {
       if (m>0){
         endStars += "*";
       }
       System.out.println(begStars + firstBlank + midStars + secondBlank + endStars);
      }
    
    System.out.println(begStars + firstBlank + midStars + secondBlank + endStars); */
     
    
    
    
    
    //SECOND ATTEMPT
    /*
    for (int i=0; i<=(gen/2.0); i++) {
      for(int j=0; j<i; j++){ //number of beginning stars will only go up to half of the number entered
        if (j>0) {
         begStars += "*";
        }
        System.out.println(begStars);
      }
      //System.out.println(begStars + firstBlank + midStars + secondBlank + endStars);
    } */
    
    
    
    //THIRD ATTEMPT
    
    //first line ONLY
    for (int i=1; i<(gen); i++){ //create the first line by making a line of stars one less than the number inputted
      midStars+="*";
    }
     System.out.println(firstBlank + midStars + lastBlank);  //print first line
    
    
    
    
    
    //up to but not including line with one blank
     for (int j=1; j<(gen/2.0); j++) { //make sure it doesnt go to line with only one blank
       begStars += "*";
       endStars += "*";
       if ((midStars.length()-2)<=0) { //when midstars should be one, make it equal to one or else error
         midStars = "*";
       }
       else {
       midStars = midStars.substring(0, midStars.length() - 2); //remove 2 stars
       }
     System.out.println(begStars + firstBlank + midStars + lastBlank + endStars); //print first middle half of lines
     } 
    
    begStars = ""; //reset strings for  middle line and second half
    endStars = "";
    midStars = "";
    
    
    
    
    
    
    //middle line ONLY
    for (int k=0; k<(gen/2.0); k++) { //create middle line by making first half and second half of stars exactly half the number inputted
      begStars += "*";
      endStars += "*";
    }
    System.out.println(begStars + midBlank + endStars); //print middle line
    
    
    
    
    
    
    //starting after middle line and going to but not including lastBlank
    for (int j=1; j<(gen/2.0); j++) { //print second half of lines
      begStars = begStars.substring(0, begStars.length() - 1); //reduce beginning and end stars by one
      endStars = endStars.substring(0, endStars.length() - 1);
      if ((midStars.length()-2)<=0) { //for the first line, make midStars 1
        midStars = "*";
      }
      System.out.println(begStars + firstBlank + midStars + lastBlank + endStars); //print lines
      midStars += "**"; //add 2 stars after the first one
     }
    
    
   //LAST LINE
    midStars = ""; //reset midStars
    for (int i=1; i<(gen); i++){ //make them up to one less of the number inputted
      midStars+="*";
    }
     System.out.println(firstBlank + midStars + lastBlank);  //print last line
    
    
    
  }
}

