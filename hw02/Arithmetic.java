////////////////////
//// CSE 02 HW 2
/// 
public class Arithmetic{
  
  public static void main(String args[]){

  //the tax rate
  double paSalesTax = 0.06;

   //Number of pairs of pants
  int numPants = 3;
  //Cost per pair of pants
  double pantsPrice = 34.98;
  //cost of all pants
  double totalPantsPrice;
  totalPantsPrice = numPants * pantsPrice;
  //sales tax on pants 
  double taxOnPants; 
  taxOnPants = paSalesTax * totalPantsPrice;
    
  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;
  //cost of all shirts
  double totalShirtsPrice;
  totalShirtsPrice = numShirts * shirtPrice;
  //sales tax on shirts 
  double taxOnShirts; 
  taxOnShirts = paSalesTax * totalShirtsPrice;

  //Number of belts
  int numBelts = 1;
  //cost per belt
  double beltCost = 33.99;
  //cost of all belts
  double totalBeltsPrice;
  totalBeltsPrice = numBelts * beltCost;
  //sales tax on belts 
  double taxOnBelts; 
  taxOnBelts = paSalesTax * totalBeltsPrice;

  double totalCostBeforeTax = totalPantsPrice + totalShirtsPrice + totalBeltsPrice;
  double totalSalesTax = taxOnPants + taxOnShirts + taxOnBelts;
  double totalCostAfterTax = totalCostBeforeTax + totalSalesTax;
    
    
    
  System.out.println("The cost of a pair of pants is $" + ((((int)(100*pantsPrice))/100.0)+0.01));//this 0.01 accounts for the inaccurate round down of a cent
  System.out.println("The cost of a shirt is $" + ((int)(100*shirtPrice))/100.0);
  System.out.println("The cost of a belt is $" + ((int)(100*beltCost))/100.0);
  System.out.println("The sales tax on 3 pairs of pants is $" + ((((int)(100*taxOnPants))/100.0)+0.01)); //this 0.01 accounts for the inaccurate round down of a cent
  System.out.println("The sales tax on 2 shirts is $" + ((((int)(100*taxOnShirts))/100.0)+0.01));//this 0.01 accounts for the inaccurate round down of a cent
  System.out.println("The sales tax on 1 belt is $" + ((int)(100*taxOnBelts))/100.0);
  System.out.println("The total cost before tax: $" + ((int)(100*totalCostBeforeTax))/100.0);
  System.out.println("The total sales tax: $" + ((int)(100*totalSalesTax))/100.0);
  System.out.println("The total cost after tax: $" + ((int)(100*totalCostAfterTax))/100.0);

  }
}