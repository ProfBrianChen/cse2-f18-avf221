///////////////////
/////CSE 02 Lab 6 
//////////////////

import java.util.Scanner;

public class PatternD{
  public static void main(String args[]){
   
    
    Scanner scan = new Scanner(System.in); //create scanner
    int num = 0; //variable for input
    //int j = 0;
    String junk = new String(); //junk string if user inputs string
    String pattern = new String(" "); //space string
   
    System.out.print("Enter an integer between 1 and 10 inclusive: "); //prompt
    while (!(scan.hasNextInt())) { //while the entered item is not an integer
      System.out.println("Error. Not valid. "); //error
      junk = scan.next();  //remove
      System.out.print("Enter an integer between 1 and 10 inclusive: ");  //reprompt
      }
    num = scan.nextInt(); //rescan
    while(num<1 || num>10) { //not a valid integer
      System.out.println("Error. Not valid. "); //error
      System.out.print("Enter an integer between 1 and 10 inclusive: ");  //reprompt
      num = scan.nextInt(); //rescan
    }
    

    System.out.println("You entered: " + num ); //print
    
    
    
   for (int j=num; j>0; j--) {    //starting at number entered
    for (int i=j; i>0; i--){  //reducing starting point by one each line
      System.out.print(i+pattern); //print integer then space
      //pattern += i;
      //pattern += " ";
    }
     System.out.println(); //start new line
   }
  }
}    
    