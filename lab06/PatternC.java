///////////////////
/////CSE 02 Lab 6 
//////////////////

import java.util.Scanner;

public class PatternC{
  public static void main(String args[]){
   
    
    Scanner scan = new Scanner(System.in); //create scanner 
    int num = 0; //variable for input num
    //int j = 0;
    String junk = new String();
    String pattern = new String("          "); //start with 10 spaces because that's the max spaces we would need
   
    System.out.print("Enter an integer between 1 and 10 inclusive: "); //prompt
    while (!(scan.hasNextInt())) { //while the entered item is not an integer
      System.out.println("Error. Not valid. "); //error
      junk = scan.next();  //remove
      System.out.print("Enter an integer between 1 and 10 inclusive: ");  //reprompt
      }
    num = scan.nextInt(); //scan next input
    while(num<1 || num>10) {
      System.out.println("Error. Not valid. "); //error
      System.out.print("Enter an integer between 1 and 10 inclusive: ");  //reprompt
      num = scan.nextInt(); //scan next input
    }
    

    System.out.println("You entered: " + num ); //print
    
    
    
   for (int j=1; j<=num; j++) { //increase starting point until we start with the input number
     System.out.print(pattern); //print out number of spaces we will need
    for (int i=j; i>0; i--){
      System.out.print(i);
      //pattern += i;
      //pattern += " ";
    }
     System.out.println(); //go to new line
     pattern = pattern.substring(0, pattern.length() - 1); //remove a space each time so that ones line up with each other
   }
  }
}    
    