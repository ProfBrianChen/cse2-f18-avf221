///////////////////
/////CSE 02 Lab 6 
//////////////////

import java.util.Scanner;

public class PatternB{
  public static void main(String args[]){
   
    
    Scanner scan = new Scanner(System.in); //create scanner
    int num = 0; //create variable for input num
    //int j = 0;
    String junk = new String(); //junk string for input
    String pattern = new String(" "); //adding a space after each print
   
    System.out.print("Enter an integer between 1 and 10 inclusive: "); //prompt
    while (!(scan.hasNextInt())) { //while the entered item is not an integer
      System.out.println("Error. Not valid. "); //error
      junk = scan.next();  //remove
      System.out.print("Enter an integer between 1 and 10 inclusive: ");  //reprompt
      }
    num = scan.nextInt(); //scan input
    while(num<1 || num>10) { //if not valid
      System.out.println("Error. Not valid. "); //error
      System.out.print("Enter an integer between 1 and 10 inclusive: ");  //reprompt
      num = scan.nextInt(); //scan next  input
    }
    

    System.out.println("You entered: " + num ); //print
    
    
    /*for (int i=num; i>0; i--) {
            for (int k=i; k>0; k--){
              System.out.print(num-k+1);
              pattern += num-k+1;
              pattern += " ";
             }
      System.out.println();
   }*/
    
   for (int j=num; j>0; j--) {     //going all the way to the number that was input, but reducing by one every time
    for (int i=1; i<=j; i++){ //printing one to the input number for each line
      System.out.print(i + pattern);
      //pattern += i;
      //pattern += " ";
    }
     System.out.println(); //new line after nested for loop
   }
  }
}    
    