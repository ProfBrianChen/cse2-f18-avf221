////////////////////
////// CSE 02 Lab 05
////////////////////

import java.util.Scanner;


public class ErrorLoops{
  public static void main(String args[]){
    int courseNum = 0; //initialize all variables that will be scanned in
    String dptName = new String();
    int timesMeet = 0;
    int timeStart = 0;
    String profName = new String();
    int numStudents = 0;
    String junk = new String();
    
    Scanner scan = new Scanner(System.in); //create scanner
    
    
    System.out.print("Enter your course number: "); //print input statement
    while (!(scan.hasNextInt())) { //while the entered item is not an integer
      System.out.println("Error. Not an int. "); //error
      junk = scan.next();  //remove
      System.out.print("Enter your course number: ");  //reprompt
      //scan.hasNextInt();
      }
    courseNum = scan.nextInt();
    System.out.println("Course number: " + courseNum ); //print
  
    
    
    System.out.print("Enter the department name: "); //print input stmt
    while ((scan.hasNextInt())) { //while the entered item is not a String
      System.out.println("Error. Not a string."); //error
      junk = scan.next(); //remove
      System.out.print("Enter the department name: "); //reprompt
      //scan.hasNextInt();
      }
    dptName = scan.next();
    System.out.println("Department name: " + dptName ); //print 
  
    
    
    System.out.print("Number times you meet per week: "); //print input statement
    while (!(scan.hasNextInt())) { //while the entered item is not an integer
      System.out.println("Error. Not an int. "); //error
      junk = scan.next(); //remove
      System.out.print("Enter number times you meet per week: "); //reprompt
      //scan.hasNextInt();
      }
      timesMeet = scan.nextInt();
    System.out.println("Times met per week: " + timesMeet ); //print 
    

    
    
    System.out.print("Enter time class starts in 24 hour time (i.e. 1pm is 1300): "); //print input statement
    while (!(scan.hasNextInt())) { //while the entered item is not an integer
      System.out.println("Error. Not an int. "); //print error
      junk = scan.next(); //remove
      System.out.print("Enter time class starts: "); //reprompt
      //scan.hasNextInt();
      }
      timeStart = scan.nextInt();
    System.out.println("Class starts at: " + timeStart ); //print 
    
    
    
    System.out.print("Enter the instructor's name: "); //print input stmt
    while ((scan.hasNextInt())) { //while the entered item is not a String
      System.out.println("Error. Not a string."); //print error
      junk = scan.next(); //remove
      System.out.print("Enter the instructor's name: "); //reprompt
      //scan.hasNextInt();
      }
    profName = scan.next();
    System.out.println("Instructor name: " + profName ); //print
    
    
    System.out.print("Number of students in class: "); //print input statement
    while (!(scan.hasNextInt())) { //while the entered item is not an integer
      System.out.println("Error. Not an int. "); //print error
      junk = scan.next(); //remove
      System.out.print("Enter number of students in class: "); //reprompt
      //scan.hasNextInt();
      }
      numStudents = scan.nextInt();
    System.out.println("Number of Students: " + numStudents ); //print 
    
    System.out.println();
    System.out.println("Here is your information.");
    System.out.println("Course number: " + courseNum ); //print all
    System.out.println("Department name: " + dptName );
    System.out.println("Times met per week: " + timesMeet );
    System.out.println("Class starts at: " + timeStart );
    System.out.println("Instructor name: " + profName );
    System.out.println("Number of Students: " + numStudents );
  }
}