////////////////////
//// CSE 02 Welcome Homework 1
/// 
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints personalized welcome message to terminal window with Lehigh ID
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-5--6--6--2--8--3->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("v  v  v  v  v  v"); 
    //autobiographical statement
    System.out.println("My name is Anna and I am on the swim team and I like getting food and snacks delivered."); 
  }
  
}