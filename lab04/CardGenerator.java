/////////////////
//////// CSE 02 Lab 4 Cards
//////
import java.util.Random;//import Random 

public class CardGenerator{
  
  public static void main(String args[] ){
    
    Random rand = new Random();//create random generator
    int card = rand.nextInt(52)+1;//pick number between 1 and 52
    String suit = new String("");//create suit string
    String identity = new String("");//create identity string
   
    if (card > 0 && card < 14) { // if card is diamond
      suit = "diamonds"; //assign suit
    switch(card){ //switch statements to assign identity
      case 1 : 
        identity = "ace";
        break;
      case 2 :
        identity = "2";
        break;
      case 3 :
        identity = "3";
        break;
      case 4 :
        identity = "4";
        break;
      case 5 :
        identity = "5";
        break;
      case 6 :
        identity = "6";
        break;
      case 7 :
        identity = "7";
        break;
      case 8 :
        identity = "8";
        break;
      case 9 :
        identity = "9";
        break;
      case 10 :
        identity = "10";
        break;
      case 11 :
        identity = "jack";
        break;
      case 12 :
        identity = "queen";
        break;
      case 13 :
        identity = "king";        
    }
    }
    else if (card > 13 && card < 27) { //if card is club
      suit = "clubs"; //assign suit
    switch(card){ //assign identity
      case 14 :
        identity = "ace";
        break;
      case 15 :
        identity = "2";
        break;
      case 16 :
        identity = "3";
        break;
      case 17 :
        identity = "4";
        break;
      case 18 :
        identity = "5";
        break;
      case 19 :
        identity = "6";
        break;
      case 20 :
        identity = "7";
        break;
      case 21 :
        identity = "8";
        break;
      case 22 :
        identity = "9";
        break;
      case 23 :
        identity = "10";
        break;
      case 24 :
        identity = "jack";
        break;
      case 25 :
        identity = "queen";
        break;
      case 26 :
        identity = "king";              
    }
    }
    else if (card > 26 && card < 40){ //if card is hearts
      suit = "hearts"; //assign suit
    switch(card){ // assign identity
      case 27 :
        identity = "ace";
        break;
      case 28 :
        identity = "2";
        break;
      case 29 :
        identity = "3";
        break;
      case 30 :
        identity = "4";
        break;
      case 31 :
        identity = "5";
        break;
      case 32 :
        identity = "6";
        break;
      case 33 :
        identity = "7";
        break;
      case 34 :
        identity = "8";
        break;
      case 35 :
        identity = "9";
        break;
      case 36 :
        identity = "10";
        break;
      case 37 :
        identity = "jack";
        break;
      case 38 :
        identity = "queen";
        break;
      case 39 :
        identity = "king";        
    }
    }
    else { //all other numbers, meaning it is spaces
      suit = "spades"; // assign suit
    switch(card){
      case 40 :
        identity = "ace";
        break;
      case 41 :
        identity = "2";
        break;
      case 42 :
        identity = "3";
        break;
      case 43 :
        identity = "4";
        break;
      case 44 :
        identity = "5";
        break;
      case 45 :
        identity = "6";
        break;
      case 46 :
        identity = "7";
        break;
      case 47 :
        identity = "8";
        break;
      case 48 :
        identity = "9";
        break;
      case 49 :
        identity = "10";
        break;
      case 50 :
        identity = "jack";
        break;
      case 51 :
        identity = "queen";
        break;
      case 52 :
        identity = "king";        
      
    }
    }
      
    System.out.println("Card number: " + card); //print card number
    System.out.println("You picked the " + identity + " of " + suit); //print identity and suit 
    
    
    
    
  }
}