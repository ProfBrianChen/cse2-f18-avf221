////////////////////////////////////////
//// CSE 02 Lab Arrays Anna Francisco
////////////////////////////////////////
import java.util.Random; //import random class

public class Arrays{
  
  public static void main(String args[]){
    int[] first = new int[100]; //create two arrays of size 100
    int[] second = new int[100]; //second might not be size 100 but thats the max it could be
    Random rand = new Random(); //random generator
    int k; //initialize k so that it can be used outside of for loop
    
    System.out.print("Array 1 holds the following integers: "); //print stmt
    
    for (int i=0; i<100; i++){ //assign random ints to first array 
      first[i]= (int) (Math.random()*99); //getting random int using math.random
      System.out.print(first[i]); //print integer at array index i
      System.out.print(" "); //space between integers
    }
    
    System.out.println(); //new line
    
    
    for (int j=0; j<100; j++){ //check each index of first array for matching numbers
      k=first[j]; //set a number to check throughout
      int count = 0; //initialize counter
      for (int m=j; m<100; m++){ //start at the next number because first numbers have been tallied already
        if (k==first[m]){ //if it matches the checking number
          count++; //add 1 to counter
      }
      }
       
      second[j]=count; //create second array
      System.out.println(k + " occurs " + second[j] + " times"); //print occurences
      
    }
    
    
   
    
    
  }
  
  
  
}