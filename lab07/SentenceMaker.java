////////////////////////////// //////////////////////
///////// CSE 02 Lab 7 Anna Francisco Sentence Maker
/////////////////////////////////////////////////////
import java.util.Scanner; //import scanner and random class
import java.util.Random; 

public class SentenceMaker{
  public static void main(String args[]){
    
    Scanner scan = new Scanner(System.in); //create scanner
    //String subject = new String("");
    
   
    
    System.out.print("If you would like to create a sentence, enter 0: "); //first prompt
    int temp = scan.nextInt();
    System.out.println();
    
    while (temp == 0) { //stay while user wants to keep printing sentences
    String a = adjective(); //creating each word by invoking method then putting into string
    String b = adjective();
    String c = subject();
    String d = verb();
    String e = adjective2();
    String f = object();
      System.out.print("The " + a + " " + b + " " + c + " " + d + " the " + e + " " + f + "."); //print the first sentence
      System.out.println(); //new line
      System.out.println(secondSentence()); //second sentence
      System.out.println(conclusion(c)); //conclusion using same subject as first sentence
      System.out.print("If you would like to create another sentence, enter 0: "); //reprompt
      temp = scan.nextInt();
      System.out.println();
    }
  }

  
  
  public static String adjective(){ //create random adjective
    Random rand = new Random(); //create random object
    int i = rand.nextInt(10); //random int 0-9
    String adjString = new String(""); //return string for subject
    switch (i) { //switch cases for subject
      case 0 : adjString = "brunette";
              break;
      case 1 : adjString = "tan";
              break;
      case 2 : adjString = "blonde";
              break;
      case 3 : adjString = "fast";
              break;
      case 4 : adjString = "smart";
              break;
      case 5 : adjString = "tall";
              break;
      case 6 : adjString = "strong";
              break;
      case 7 : adjString = "creative";
              break;
      case 8 : adjString = "observant";
              break;
      case 9 : adjString = "loud";
              break;
    }
    
    return adjString;
  }
    public static String adjective2(){ //create random adjective for the object
    Random rand5 = new Random(); //random generator
    int i = rand5.nextInt(10); //0-9
    String adjString2 = new String("");
    switch (i) { //adjective
      case 0 : adjString2 = "red";
              break;
      case 1 : adjString2 = "yellow";
              break;
      case 2 : adjString2 = "colorful";
              break;
      case 3 : adjString2 = "big";
              break;
      case 4 : adjString2 = "detailed";
              break;
      case 5 : adjString2 = "vintage";
              break;
      case 6 : adjString2 = "new";
              break;
      case 7 : adjString2 = "modern";
              break;
      case 8 : adjString2 = "bright";
              break;
      case 9 : adjString2 = "pink";
              break;
    }
    
    return adjString2;
  }
  public static String subject(){ //create random non-primary noun for subject
    Random rand1 = new Random(); //random generator
    int j = rand1.nextInt(11); //0-11
    String subjectString = new String("");
    switch (j) { //subject of first sentence and last
      case 0 : subjectString = "runner";
              break;
      case 1 : subjectString = "doctor";
              break;
      case 2 : subjectString = "student";
              break;
      case 3 : subjectString = "teacher";
              break;
      case 4 : subjectString = "gryphon";
              break;
      case 5 : subjectString = "athlete";
              break;
      case 6 : subjectString = "librarian";
              break;
      case 7 : subjectString = "chef";
              break;
      case 8 : subjectString = "friend";
              break;
      case 9 : subjectString = "stranger";
              break;
      case 10 : subjectString = "surgeon";
              break;
    }
    //subject += subjectString;
    return subjectString; //return subject string
  }
  public static String object(){ //create random non-primary noun for object
    Random rand2 = new Random(); //generator
    int k = rand2.nextInt(10); //0-9
    String objectString = new String(""); //return string
    switch (k) { //pick an object
      case 0 : 
        objectString = "letter";
        break;
      case 1 : 
        objectString = "bracelet";
        break;
      case 2 : 
        objectString = "gift";
        break;
      case 3 : 
        objectString = "box";
        break;
      case 4 : 
        objectString = "picture";
        break;
      case 5 : 
        objectString = "jacket";
        break;
      case 6 : 
        objectString = "shelf";
        break;
      case 7 : 
        objectString = "poster";
        break;
      case 8 : 
        objectString = "souvenir";
        break;
      case 9 : 
        objectString = "gift basket";
        break;
    }
    return objectString; //return
  }
  public static String verb(){ //method for random past tense verb
    Random rand3 = new Random(); //generator
    int m = rand3.nextInt(10);//create 0-9
    String verbString = new String("");
    switch (m) { // pick verb depending on random variable
      case 0 : 
        verbString = "mailed";
        break;
      case 1 : 
        verbString = "sent";
        break;
      case 2 : 
        verbString = "passed";
        break;
      case 3 : 
        verbString = "gave";
        break;
      case 4 : 
        verbString = "received";
        break;
      case 5 : 
        verbString = "borrowed";
        break;
      case 6 : 
        verbString = "loaned";
        break;
      case 7 : 
        verbString = "shipped";
        break;
      case 8 : 
        verbString = "broke";
        break;
      case 9 : 
        verbString = "inherited";
        break;
       
    }
    return verbString;
  }
  
  
  public static String secondSentence(){ //create second sentence based on template
    Random rand3 = new Random(); //generator
    String ending = new String("");
    int m = rand3.nextInt(2); //either 0 or 1
    switch (m) { //picking between he or she
      case 0 :
        ending += "She ";
        break;
      case 1 :
        ending += "He ";
        break;
    }
    ending += "used "; //only verb
    int n = rand3.nextInt(10); //pick object
    switch (n) {
      case 0 : 
        ending += "graduated cylinders ";
        break;
      case 1 : 
        ending += "water guns ";
        break;
      case 2 : 
        ending += "coffee cups ";
        break;
      case 3 : 
        ending += "mouse pads ";
        break;
      case 4 : 
        ending += "printer paper ";
        break;
      case 5 : 
        ending += "pumpkin seeds ";
        break;
      case 6 : 
        ending += "street signs ";
        break;
      case 7 : 
        ending += "shoelaces ";
        break;
      case 8 : 
        ending += "business cards ";
        break;
      case 9 : 
        ending += "bobby pins ";
        break;  
    }
    ending += "to "; //adding to to string
    int l = rand3.nextInt(5); //0-4
    switch (l) { //verb of second sentence
      case 0 : 
        ending += "fling ";
        break;
      case 1 : 
        ending += "throw ";
        break;
      case 2 : 
        ending += "launch ";
        break;
      case 3 : 
        ending += "catapult ";
        break;
      case 4 : 
        ending += "dump ";
        break;
    }
    int k = rand3.nextInt(5); //0-4 
    switch (k){ //picking indirect object
      case 0 : 
        ending += "brain matter ";
        break;
      case 1: 
        ending += "human organs ";
        break;
      case 2: 
        ending += "computer parts ";
        break;
      case 3: 
        ending += "coffee grinds ";
        break;
      case 4: 
        ending += "post-it-notes ";
    }
    ending += "at the "; //given string words
    ending += adjective(); //invoking adjective method
    ending += " ";
    ending += subject(); //invoking subject method
    ending += ".";
    return ending; //return
    
    
    
  }
  
  public static String conclusion(String c){ //create conclusion with the same subject as the first sentence, hence the input
    Random rand3 = new Random();
    String ending = new String("");
    ending += "That ";
    ending += c;
    ending += " ";
    int k = rand3.nextInt(5); //creating past tense verb
    switch (k) {
      case 0 : 
        ending += "knew ";
        break;
      case 1 : 
        ending += "showed ";
        break;
      case 2 : 
        ending += "presented ";
        break;
      case 3 : 
        ending += "loved ";
        break;
      case 4 : 
        ending += "liked ";
        break;

    }
    int n = rand3.nextInt(5); //0-4
    switch (n) { //picking object
      case 0 : 
        ending += "computers!";
        break;
      case 1 : 
        ending += "water guns!";
        break;
      case 2 : 
        ending += "coffee flavors!";
        break;
      case 3 : 
        ending += "hair colors!";
        break;
      case 4 : 
        ending += "cell phones!";
        break;

    }
    return ending;
  }
  
}


