////////////////////////////////////
///////CSE 02 HW 07 Anna Francisco
////////////////////////////////////


import java.util.Scanner;

public class WordTools{
  public static void main(String args[]){
   
   String sample = sampleText();
   System.out.print("You entered: " + sample);
   System.out.println();
   printMenu(sample);  
    
    
    
    
    
    //////////////////////////////////////////////
   //char char2 = printMenu();
   //if (char2 == 'c') {
    //System.out.println("Number of non-white space characters: " + getNumOfNonWSCharacters(sample));  
   //}
   /*else if (char2 == 'w') {
    System.out.println("Number of words: " + getNumOfWords(sample));
   }
   else if (char2 == 'f') {
    System.out.println("What word would you like to find inside of your sample string?: ");
    String find = scan.next();
    System.out.println("Number of words found in given string: " + findText(sample, find));
   }               
   else if (char2 == 'r') {
    System.out.println("Text without exclamations: " + replaceExclamation());
   }
   else {
    System.out.println("String with shortened spaces: " + shortenSpace());
   }*/
    ///////////////////////////////////////////////////////
 }
  
  
  
  
  public static String sampleText(){ //method to get sample string
    
    ///////////////////////////////////
    /*Scanner scan = new Scanner(System.in);
    String userInput = new String("");
    String temp = new String();
    System.out.print("Type a text string of your choosing, then press enter: ");*/
    //////////////////////////////////////
    
    
    
    System.out.print("Type a text string of your choosing, then press enter: ");
    String userInput = new String("");
    Scanner scan = new Scanner(System.in);
    userInput = scan.nextLine(); 
    return userInput;
    
  }
  
  
  public static char printMenu(String a){ //method to print menu and execute the option user picked
    Scanner scan1 = new Scanner(System.in);
    Boolean bool2 = true; //to stay or leave while loop
    char temp3 = ' '; //character to be returned
    while (bool2) { //while option is not valid
        System.out.println("~~~~~~~~~~~~~~~");
        System.out.println("MENU OPTIONS: ");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
        System.out.println("~~~~~~~~~~~~~~~");
        Boolean bool = false;
        //do {

          System.out.println("Choose an option by entering the character specified above: ");
          String temp1 = scan1.next();
          char temp2 = temp1.charAt(0);

          if (temp2 == 'c'){
          System.out.println("You picked number of non-whitespace characters");
          System.out.println("Number of non-white space characters: " + getNumOfNonWSCharacters(a)); 
          }
          else if (temp2 == 'w'){
          System.out.println("You picked number of words.");
          System.out.println("Number of words: " + getNumOfWords(a));
          }
      
          else if (temp2 == 'f'){
          System.out.println("You picked find text.");
          System.out.println("What word would you like to find inside of your sample string?: ");
          String find = scan1.next();
          System.out.println("Number of words found in given string: " + findText(find, a));       
          }
      
          else if (temp2 == 'r'){
          System.out.println("You picked replace all !'s");
          System.out.println("Text without exclamations: " + replaceExclamation(a));
          }
      
          else if (temp2 == 's'){
          System.out.println("You picked shorten spaces");
          System.out.println("String with shortened spaces: " + shortenSpace(a));
          }
          else if (temp2 == 'q'){
          bool2 = false;
          System.out.println("You picked quit");
          break;
          }
          else {
            //bool = true;
            System.out.println("Not a valid option.");
          }
       // } while (bool == true);
         System.out.println();
         temp3 = temp2;
        }
    return temp3;
  }
  
  
  
  
  public static int getNumOfNonWSCharacters(String a){ //method to count whitespaces
    int k = 0; //counter for white spaces
    for (int i=0; i<a.length()-1; i++) { //go over each element of string
      if (Character.isWhitespace(a.charAt(i))) { //if a whitespace
        k++;
      }
    }
    return a.length()-k; //the length of string minus whitespace is nonwhite space
 }
  
 public static int getNumOfWords(String a){ //method to get number of words
    int k = 0; //counter for words
    for (int i=0; i<a.length()-1; i++) { //go over each element of string
      if (Character.isWhitespace(a.charAt(i))) { //if a whitespace
        k++;
      }
    } 
    return k+1; //number of words is whitespaces plus 1
  }
  
  public static int findText(String a, String b){ //finding a word inside sample text
    int k=0;//counter for finding word
    for (int i=0; i<(b.length()-a.length()+1); i++) { //going from the first index of string to the last one possible where the find word can fit
      if (b.substring(i,i+a.length()).equals(a)) { //taking each substring the length of the find word
        k++;
      }
    }
    return k;
  }
  
  
  
  public static String replaceExclamation(String a){ //method for replacing exclamation
    String b = a; //set another string equal to original a, so that we still have a string with the original when we recreate a
    for (int i=0; i<a.length(); i++) {
     if (a.charAt(i) == '!') {
       if (i==0) {
         a = '.' + b.substring(1, a.length()); //if it is the first character
       }
       else {
         a = a.substring(0, i) + "." + b.substring(i+1, a.length());
       }
     }
    }
    return a;
  }
  
  public static String shortenSpace(String a){ //shorten spaces
    String b = a;
    
    for (int i=0; i<a.length()-3; i++) { //create a for loop to round through indexes of string and find spaces
     int k=0;
     if (a.charAt(i) == ' ' && a.charAt(i+1) == ' ') {
       if (i==0) {
         a = " " + b.substring(2, a.length()); //reduce to one space
         k++;
       }
       else {
         a = a.substring(0, i) + " " + a.substring(i+2, a.length()); //reduce to one space
         k++;
       }
     }
     if (a.charAt(i+2) == ' ' && a.charAt(i+3) == ' ' && k>0) { //if there are more than two spaces
       if (i==0) {
         a = b.substring(2, a.length());
         k++;
       }
       else {
         a = a.substring(0, i) + a.substring(i+2, a.length());
         k++;
       }
     }

    }
    return a;
  }
  
  
  
  
  
  
  
  
}