////////////////////
//// CSE 02 HW 3 Pyramid
///  
import java.util.Scanner;
public class Pyramid{
  
  public static void main(String args[]){
    
    Scanner askUser = new Scanner( System.in );//create scanner called askUser
    System.out.print("The square side of the pyramid in feet is: ");//print input statement for length
    double squareLength = askUser.nextDouble();//create double called squareLength
    System.out.print("Enter the height of the pyramid in feet: ");//print input statement for pyramid height
    double height = askUser.nextDouble();//create double called height with input
    double volume = squareLength * squareLength * height / 3.0; //compute volume
    System.out.println("The volume of the pyramid is " + volume + " cubic feet");//print volume
    
    

    
  }
}