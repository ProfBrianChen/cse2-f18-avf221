////////////////////
//// CSE 02 HW 3
/// 
import java.util.Scanner;
public class Convert{
  
  public static void main(String args[]){
    
    Scanner askUser = new Scanner( System.in );//create scanner called askUser
    System.out.print("Enter the affected area in acres: ");//print input statement for acres
    double acres = askUser.nextDouble();//create double called acres with input
    System.out.print("Enter the rainfall in the affected area in inches: ");//print input statement for rainfall
    double rainfall = askUser.nextDouble();//create double called rainfall with input
    double gallonsPerAcreInch = 27154.2857; //conversion factor for acre inches to gallons
    double gallons = acres * rainfall * gallonsPerAcreInch; //convert to gallons
    double cubicMilesPerAI = 9.08169E-13; //conversion factor for cubicMilesPerAI
    double cubicMiles = gallons * cubicMilesPerAI; //covert to cubic miles
    System.out.println("The area affected is a total of " + cubicMiles + " cubic miles");
    
    

    
  }
}